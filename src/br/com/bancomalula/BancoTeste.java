package br.com.bancomalula;

public class BancoTeste {
	
	public static void main(String[] args) {
				
		Conta.saldoDoBanco = 2_000.00;
		
		Conta conta = new Conta(1, "Corrente", 1_500, "123!", new Cliente
			("Irineu", "89.125.458-6", "156.568.321-59", "irinenheu@gmail.com", Sexo.MASC));

		conta.exibeSaldo();
		conta.deposita(50);
		conta.exibeSaldo();
		System.out.println("Cofre" + Conta.saldoDoBanco);
		conta.saca(100);
		conta.exibeSaldo();
		System.out.println("Cofre" + Conta.saldoDoBanco);
		
		// mostrar os dados da conta
		System.out.println("DADOS DA CONTA:");
		System.out.println("N�       " + conta.getNumero());
		System.out.println("Tipo:    " + conta.getTipo());
		System.out.println("Senha:   " + conta.getSenha());
		System.out.println("Saldo:   " + conta.getSaldo());
		System.out.println("Titular: " + conta.getCliente().getNome()); // Quando est� em outra classe
		System.out.println("Titular: " + conta.getCliente().getCpf());
		System.out.println("Titular: " + conta.getCliente().getRg());
		System.out.println("Titular: " + conta.getCliente().getEmail());
		System.out.println("Titular: " + conta.getCliente().getSexo().nome); // Enum
	}
}
